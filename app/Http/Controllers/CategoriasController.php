<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorias;

class CategoriasController extends Controller
{
      public function __construct()
      {
          $this->middleware('auth');
      }

      public function index() {
        $categorias = Categorias::all();
        return view('panel/categorias', ['categorias' => $categorias]);
      }

      public function indexPost(Request $request) {

        $categorias = new Categorias;
        $categorias->nombre_categoria = $request['nombre_categoria'];
        $categorias->save();

        return redirect()->back();
      }

      public function destroy($id) {
        Categorias::destroy($id);
        return redirect()->back()->with('status', 'La categoria ha sido eliminada exitosamente.');
      }

      public function update($id) {
        $categorias = Categorias::find($id)->get();
        return view('panel/categoriasUpdate', ['categorias' => $categorias]);
      }

      public function updatePost(Request $request) {
        $categorias = Categorias::find($request['id_categoria']);
        $categorias->nombre_categoria = $request['nombre_categoria'];
        $categorias->save();
        return redirect('categorias')->with('status', 'La categoria ha sido editada exitosamente.');;
      }

}
