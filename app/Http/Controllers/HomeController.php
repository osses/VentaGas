<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Despachos;
use App\Camiones;
use App\Productos;
use App\Productos_camiones;
use App\Productos_facturas;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $despachos = Despachos::where('estado', 1)->orderBy('created_at', 'DESC')->limit(5)->get();
        return view('home', ['despachos' => $despachos]);
    }

    public function usuarios() {
      $users = DB::table('users')->get();
      return view('panel/usuarios', ['usuarios' => $users]);
    }

    public function update($id){
      $user = User::where('id', $id)->get();
      return view('panel/editarUsuario', ['usuarios' => $user]);
    }
    public function updatePost(Request $request){

      $user = User::find($request['id_usuario']);
      $user->name = $request['name'];
      $user->email = $request['email'];
      if($request['password']){
        $user->password = bcrypt($request['password']);
      }
      $user->save();

      return redirect('/usuarios')->with('status', 'El usuario ha sido editado correctamente');
    }

    public function devolverStock() {

      $productosCamiones = DB::table('productos_camiones')->get();

      foreach ($productosCamiones as $value) {

        $update_up = DB::table('productos')
          ->where('nombre_producto', $value->nombre_producto)
          ->increment('stock', $value->stock);

        if($update_up) {
            $update_down = DB::table('productos_camiones')
              ->update(['stock' => 0]);
          }

      }

      return redirect('/camiones')->with('status', 'El stock se devolvio a bodega exitosamente.');

    }

    public function devolverStockCamion($id_camion) {

      $productosCamiones = DB::table('productos_camiones')->where('fk_id_camion', $id_camion)->get();

      foreach ($productosCamiones as $value) {

            $update_up = DB::table('productos')
              ->where('nombre_producto', $value->nombre_producto)
              ->increment('stock', $value->stock);

                $update_down = DB::table('productos_camiones')
                ->where('nombre_producto', $value->nombre_producto)
                ->where('fk_id_camion', $id_camion)
                ->update(['stock' => 0]);
          }

      return redirect('/camiones')->with('status', 'El stock se devolvio a bodega exitosamente.');

    }

    public function IngresarFalla(){
      $productos = DB::table('productos')->get();
      $productos_fallas = DB::table('productos_fallas')->get();
      return view('panel/fallas', ['productos' => $productos, 'productos_fallas' => $productos_fallas]);

    }

    public function DevolveraStock(Request $request){

        $productos_fallas = DB::table('productos_fallas')
        ->where('fk_id_producto', $request['id_producto'])
        ->decrement('stock', $request['cantidad_falla']);

        return redirect('/IngresarFallas')->with('status', 'Los cilindros se devolvieron correctamente');
    }

    public function IngresarFallaPost(Request $request){

      $buscar = DB::table('productos_fallas')->where('fk_id_producto', $request['id_producto'])->count();

      if(!$buscar) {
        $insert = DB::table('productos_fallas')->insert(
                  ['fk_id_producto' => $request['id_producto'], 'stock' => $request['cantidad']]
              );
      }
      else {
        $insert = DB::table('productos_fallas')
        ->where('fk_id_producto', $request['id_producto'])
        ->increment('stock', $request['cantidad']);
      }

      return redirect('/IngresarFallas')->with('status', 'La falla se ha ingresado correctamente.');
    }

    public function reportes() {
      $despachos = Despachos::where('estado', 1)->orderBy('created_at', 'DESC')->limit(5)->get();
      return view('panel/reportes', ['despachos' => $despachos]);
    }

    public function camiones() {
      $camiones = DB::table('camiones')->get();
      return view('panel/camiones', ['camiones' => $camiones]);
    }

    public function updateCamion($id) {
      $camion = DB::table('camiones')->where('id', $id)->limit(1)->get();
      $usuarios = DB::table('users')->where('tipo_usuario', 1)->get();
      return view('panel/editarCamion', ['camiones' => $camion, 'usuarios' => $usuarios]);
    }

    public function updateCamionPost(Request $request) {

          $camion = Camiones::find($request['id_camion']);
          $camion->marca = $request['marca'];
          $camion->patente = $request['patente'];
          $camion->fk_id_chofer = $request['chofer'];
          $camion->save();

          return redirect('camiones')->with('status', 'El camion ha sido editado correctamente..');
    }

    public function stockCamion($id) {
        $stock = DB::table('productos_camiones')->where('fk_id_camion', $id)->get();
        $camion = DB::table('camiones')->where('id', $id)->limit(1)->get();
        return view('panel/stockCamiones', ['stocks' => $stock, 'camiones' => $camion]);
    }

    public function asignarStock() {
      $camion = DB::table('camiones')->get();
      $productos = DB::table('productos')->get();
      return view('panel/asignarStock', ['camiones' => $camion, 'productos' => $productos]);
    }

    public function asignarStockPost(Request $request) {

        // verificar si existe el producto sino crearlo

        $find = DB::table('productos_camiones')
        ->where('fk_id_camion', $request['id_camion'])
        ->where('nombre_producto', $request['nombre_producto'])
        ->count();

        if($find <= 0) {

          $insert = New Productos_camiones;
          $insert->nombre_producto = $request['nombre_producto'];
          $insert->fk_id_categoria = 1;
          $insert->fk_id_camion = $request['id_camion'];
          $insert->stock = $request['cantidad'];
          $insert->save();

          $quitar = DB::table('productos')
          ->where('nombre_producto', $request['nombre_producto'])
          ->decrement('stock', $request['cantidad']);

          return redirect()->back();

        } else {

          $update = DB::table('productos_camiones')
              ->where('fk_id_camion', $request['id_camion'])
              ->where('nombre_producto', $request['nombre_producto'])
              ->increment('stock', $request['cantidad']);

          if($update) {
            $quitar = DB::table('productos')
            ->where('nombre_producto', $request['nombre_producto'])
            ->decrement('stock', $request['cantidad']);
          }

          return redirect()->back()->with('status', 'El stock se asigno correctamente.');
        }
    }

    public function facturas() {
      $productos = Productos::all();
      return view('panel/facturas', ['productos' => $productos]);
    }

    public function facturasPost(Request $request) {

      foreach ($request['nombre_producto'] as $producto) {
            $productos[] = $producto;
      }

      foreach ($request['cantidad'] as $cantidad) {
            $cantidades[] = $cantidad;
      }

      // INSERTANDO EN PRODUCTOS FACTURAS
      for ($i=0; $i < count($request['nombre_producto']); $i++) {

            // INSERTANDO STOCK EN PRODUCTOS
        $update = DB::table('productos')
                ->where('nombre_producto', $productos[$i])
                ->increment('stock', $cantidades[$i]);

          $insert = New Productos_facturas;
          $insert->numero_factura = $request['nfactura'];
          $insert->nombre_producto = $productos[$i];
          $insert->fk_id_categoria = 1;
          $insert->stock = $cantidades[$i];
          $insert->save();
      }

      return redirect()->back()->with('status', 'La factura se ingreso correctamente.');;

    }

    public function verFacturas() {
      $facturas = DB::table('productos_facturas')->get();
      return view('panel/verFacturas', ['facturas' => $facturas]);
    }

    public function verFacturasId($numero_factura) {

      $factura = DB::table('productos_facturas')
      ->where('numero_factura', $numero_factura)
      ->limit(1)->get();

      $factura_productos = DB::table('productos_facturas')
      ->where('numero_factura', $numero_factura)
      ->get();
      return view('panel/factura', ['facturas' => $factura, 'productos' => $factura_productos]);
    }
}
