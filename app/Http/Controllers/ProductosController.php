<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productos;
use App\Categorias;
use Image;

class ProductosController extends Controller
{
    public function index() {
      $productos = Productos::all();
      $productos_sub = Productos::all();
      $categorias = Categorias::all();
      return view('panel/productos', ['productos' => $productos, 'productos_sub' => $productos_sub, 'categorias' => $categorias]);
    }

    public function indexPost(Request $request) {

        /* if($request['imagen']) {
        $image_muestra = Image::make($request['imagen']);
        $newNameMuestra = mt_rand() .time().'.'.$request['imagen']->getClientOriginalExtension();
        $image_muestra->fit(200, 200);
        $image_muestra->save('images/productos/'.$newNameMuestra);*/

        $productos = new Productos;
        $productos->nombre_producto = $request['nombre_producto'];
        // $productos->imagen_producto = 'images/productos/'.$newNameMuestra;
        //$productos->descripcion_producto = $request['descripcion_producto'];
        $productos->fk_id_categoria = $request['fk_id_categoria'];
        $productos->stock = $request['stock'];
        $productos->save();

        return redirect()->back()->with('status', 'El producto se creado correctamente.');

       // }
     }

     public function update($id) {
        $productos = Productos::where('id', $id)->get();
        $categorias = Categorias::all();
        return view('panel/editarproductos', ['productos' => $productos, 'categorias' => $categorias]);
     }

     public function updatePost(Request $request) {

         //if($request['imagen']) {
         //$image_muestra = Image::make($request['imagen']);
         //$newNameMuestra = mt_rand() .time().'.'.$request['imagen']->getClientOriginalExtension();
         //$image_muestra->fit(200, 200);
         //$image_muestra->save('images/productos/'.$newNameMuestra);

         $productos = Productos::find($request['id']);
         $productos->nombre_producto = $request['nombre_producto'];
         //$productos->imagen_producto = 'images/productos/'.$newNameMuestra;
         //$productos->descripcion_producto = $request['descripcion_producto'];
         $productos->fk_id_categoria = $request['fk_id_categoria'];
         $productos->stock = $request['stock'];
         $productos->save();

         return redirect()->back()->with('status', 'El producto ha sido editado correctamente.');

         //  }
      }

      public function destroy($id) {
        Productos::destroy($id);
        return redirect()->back()->with('status', 'El producto ha sido eliminado correctamente.');
      }

      public function nuevoProducto() {
        $categorias = Categorias::all();
        return view('panel/nuevoProducto', ['categorias' => $categorias]);
      }

}
