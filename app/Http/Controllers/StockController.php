<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Productos;
use App\Categorias;
use App\Despachos;
use App\User;
use Auth;
use DB;

class StockController extends Controller
{
    public function index() {
      $productos = Productos::orderBy('stock', 'ASC')->get();
      $productos_sub = Productos::orderBy('stock', 'ASC')->get();
      $categorias = Categorias::all();
      return view('panel/stock', ['productos' => $productos, 'productos_sub' => $productos_sub, 'categorias' => $categorias]);
    }

    public function despachos() {
      $productos = Productos::all();
      $users = User::where('tipo_usuario', 1)->get();
      $metodos_pago = DB::table('metodo_pago')->get();
      return view('panel/despachos', ['usuarios' => $users, 'productos' => $productos, 'metodos_pago' => $metodos_pago]);
    }

    public function despachosPost(Request $request){

        $despachos = new Despachos;
        $despachos->fk_id_usuario = $request['id_usuario'];
        $despachos->save();

        foreach ($request['producto'] as $producto) {
              $productos[] = $producto;
        }

        foreach ($request['cantidad'] as $cantidad) {
              $cantidades[] = $cantidad;
        }

        foreach ($request['camiones'] as $camion) {
              $camiones[] = $camion;
        }

        foreach ($request['metodo_pago'] as $metodo_pago) {
              $metodopago[] = $metodo_pago;
        }

        foreach ($request['direccion'] as $address) {
              $direccion[] = $address;
        }

        for ($i=0; $i < count($request['producto']); $i++) {
            if($cantidades[$i]) {
              $insert = DB::table('productos_despachados')->insert(
                        ['fk_id_producto' => $productos[$i], 'cantidad' => $cantidades[$i], 'fk_id_despacho' => $despachos->id, 'direccion' => $direccion[$i], 'metodo_pago' => $metodopago[$i]]
                    );

              $update = DB::table('productos_camiones')
                ->where('fk_id_camion', $camiones[$i])
                ->decrement('stock', $cantidades[$i]);

                // INSERTANDO LOGS
                DB::table('log_ventas')->insert([
                    ['fk_id_producto' => $productos[$i], 'fk_id_usuario' => Auth::User()->id, 'cantidad' => $cantidades[$i]]
                ]);
            }

        }
        return redirect()->back()->with('status', 'El despacho se ha asigando correctamente.');;
    }
}
