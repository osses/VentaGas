<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Despachos;
use App\Productos;
use Auth;
use DB;

class DespachosController extends Controller
{
    public function misdespachos() {
        $despachos = Despachos::where('fk_id_usuario', Auth::User()->id)->orderBy('created_at', 'DESC')->get();
        return view('panel/misdespachos', ['despachos' => $despachos]);
    }

    public function despachosCheck($id_despacho) {
      $despachos = Despachos::where('id', $id_despacho)->get();
      $productos_despachos = DB::table('productos_despachados')->where('fk_id_despacho', $id_despacho)->get();
      return view('panel/despachosCheck', ['despachos' => $despachos, 'productosDespachados' => $productos_despachos]);
    }

    public function despachosCheckAdmin() {
      $despachos = Despachos::orderBy('created_at', 'DESC')->get();
      return view('panel/despachosCheckAdmin', ['despachos' => $despachos]);
    }

    public function despachosCheckPost(Request $request) {

      foreach ($request['id_producto'] as $producto) {
            $productos[] = $producto;
      }

      foreach ($request['entregado'] as $entregado) {
            $cantidades[] = $entregado;
      }

      for ($i=0; $i < count($request['id_producto']); $i++) {
        if($request['restar_de'] == '0') {
          DB::table('productos')->where('id', $productos[$i])->decrement('stock', $cantidades[$i]);
        }
        else {
          DB::table('productos_camiones')->where('fk_id_camion', $request['restar_de'])->where('nombre_producto', ObtenerNombreProducto($productos[$i]))->decrement('stock', $cantidades[$i]);
        }

        // INSERTANDO LOGS
        DB::table('log_ventas')->insert([
            ['fk_id_producto' => $productos[$i], 'fk_id_usuario' => Auth::User()->id, 'cantidad' => $cantidades[$i]]
        ]);
      }

      DB::table('despachos')->where('id', $request['id_despacho'])->update(['estado' => 1]);

      return redirect('/checkear-despachos');
    }
}
