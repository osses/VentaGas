<?php

function ObtenerNCategoria($id) {
  $nombre = DB::table('categorias')
  ->where('id', $id)
  ->first();
  return @$nombre->nombre_categoria;
}

function StockDisponible($id){
  $stock = DB::table('productos')
  ->where('id', $id)
  ->first();
  return @$stock->stock;
}

function ProductoConFalla($id_producto) {
  $stock = DB::table('productos_fallas')
  ->where('fk_id_producto', $id_producto)
  ->first();

  return @$stock->stock;
}

function ObtenerNombreProducto($id){
  $nombre = DB::table('productos')
  ->where('id', $id)
  ->first();
  return @$nombre->nombre_producto;
}

function ObtenerDireccion($id_despacho) {
  $nombre = DB::table('productos_despachados')
  ->where('fk_id_despacho', $id_despacho)
  ->first();
  return @$nombre->direccion;
}

function DespachosPendientes() {
  $count = DB::table('despachos')
  ->where('estado', 0)->count();

  return @$count;
}

function CilindrosEntregados() {
  $count = DB::table('log_ventas')
  ->sum('cantidad');

  return @$count;
}

function CilindrosConFallas() {
  $count = DB::table('productos_fallas')
  ->sum('stock');

  return @$count;
}

function DespachosConcretados() {
  $count = DB::table('log_ventas')
  ->count();

  return @$count;
}

function UsuariosTotales() {
  $count = DB::table('users')
  ->count();

  return @$count;
}

function NombreUsuario($id) {
  $nombre = DB::table('users')
  ->where('id', $id)
  ->first();
  return @$nombre->name;
}

function NombreChofer($id_camion) {
  $nombre = DB::table('camiones')
  ->where('id', $id)
  ->first();
  return @$nombre->name;
}

function ObtenerStockCamiones($nombre_producto){
  $nombre = DB::table('productos_camiones')
  ->where('nombre_producto', $nombre_producto)
  ->sum('stock');

  return @$nombre;
}

function ObtenerStockCamionesID($nombre_producto, $id_camion){
  $nombre = DB::table('productos_camiones')
  ->where('nombre_producto', $nombre_producto)
  ->where('fk_id_camion', $id_camion)
  ->sum('stock');

  return @$nombre;
}


function ObtenerMetododePago($id) {
  $nombre = DB::table('metodo_pago')
  ->where('id', $id)
  ->first();
  return @$nombre->nombre;
}

function ObtenerMarcayPatente($id) {
  $nombre = DB::table('camiones')
  ->where('id', $id)
  ->first();

  return @$nombre->patente;
}


function ObtenerCamionesQueTenganElProducto($nombre_producto, $cantidad) {
    $nombre = DB::table('productos_camiones')
    ->where('nombre_producto', $nombre_producto)
    ->where('stock', '>=', $cantidad)
    ->get();
    $option = "";
    $option = $option."<option value='0'  selected>Seleccione Camión</option>";
  foreach($nombre as $camiones) {
    $option = $option."<option value='".$camiones->fk_id_camion."'>".ObtenerMarcayPatente($camiones->fk_id_camion)." | ".$camiones->stock."</option>";
  }
  echo $option;
}

function ObtenerCamionesQueTenganElProductoSinCantidad($nombre_producto) {
    $nombre = DB::table('productos_camiones')
    ->where('nombre_producto', $nombre_producto)
    ->get();
    $option = "";
    $option = $option."<option value='0'  selected>Seleccione Camión</option>";
  foreach($nombre as $camiones) {
    $option = $option."<option value='".$camiones->fk_id_camion."'>".ObtenerMarcayPatente($camiones->fk_id_camion)." | ".$camiones->stock."</option>";
  }
  echo $option;
}


 ?>
