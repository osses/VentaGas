<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $fillable = [
        'id', 'nombre_producto','descripcion_producto','fk_id_categoria', 'stock',
    ];
}
