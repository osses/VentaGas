<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camiones extends Model
{
  protected $fillable = [
      'id', 'marca', 'patente', 'fk_id_chofer'
  ];
}
