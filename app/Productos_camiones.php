<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos_camiones extends Model
{
    protected $fillable = [
        'id', 'nombre_producto','descripcion_producto','fk_id_categoria', 'fk_id_camion', 'stock',
    ];
}
