<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos_facturas extends Model
{
  protected $fillable = [
      'id', 'numero_factura', 'nombre_producto','descripcion_producto','fk_id_categoria', 'stock',
  ];
}
