<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Despachos extends Model
{
    protected $fillable = [
        'id', 'fk_id_usuario','created_at','updated_at','estado',
    ];
}
