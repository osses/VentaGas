@extends('layouts.app')
@section('content')
  <div class="col-md-9">
    @if (session('status'))
     <div class="alert alert-success">
       <p>{{ session('status') }}</p>
          </div>
           @endif
            <div class="panel panel-default">
                <div class="panel-heading">Usuarios</div>
                <div class="panel-body">
                  <!--
                  <a href="#" class="btn btn-success btn-sm btn3d"><i class="fas fa-user"></i> Nuevo Usuario</a>
                  <hr>
                  -->
                  <table class="table table-bordered" id="usuarios">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($usuarios as $user)
                      <tr style="background-color:#464545;">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td><a href="/editar/usuario/{{ $user->id }}" class="btn btn-info btn-xs btn3d">Editar</a> <!--| <a href="/eliminar/usuario/{{ $user->id }}" class="btn btn-danger btn-xs btn3d">Eliminar</a> --></td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
         </div>
</div>
<!--<script type="text/javascript">
$(document).ready(function(){
  $('#usuarios').DataTable( {
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
        }
    });
});
</script>-->
@endsection
