@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-heading"><a href="/devolverstock" class="btn btn-success btn-xs btn3d">Devolver Stock</a></div>
                <div class="panel-body">
                      <table class="table table-bordered">
                        <tr>
                          <td>#</td>
                          <td>Marca</td>
                          <th>Patente</th>
                          <th>Nombre Chofer</th>
                          <th>Acción</th>
                        </tr>
                        @foreach($camiones as $camion)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $camion->marca }}</td>
                          <td>{{ $camion->patente }}</td>
                          <td>{{ NombreUsuario($camion->fk_id_chofer) }}</td>
                          <td><a href="/stock/camion/{{ $camion->id }}" class="btn btn-info btn-xs btn3d">Ver Stock</a> <a href="/editar/camion/{{ $camion->id }}" class="btn btn-warning btn-xs btn3d">Editar</a>  <a href="/devolverstock/{{ $camion->id }}" class="btn btn-success btn-xs btn3d">Devolver Stock</a> </td>
                        </tr>
                        @endforeach
                      </table>
                    </div>
                  </div>
        </div>
@endsection
