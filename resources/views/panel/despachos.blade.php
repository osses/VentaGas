@extends('layouts.app')
@section('content')
<div class="col-md-9">
  @if (session('status'))
    <div class="alert alert-success">
      <p>{{ session('status') }}</p>
    </div>
  @endif
  <div class="panel panel-default">
      <div class="panel-heading">Despachos</div>
      <div class="panel-body">
        <form class="well" action="" method="post">
          {!! csrf_field() !!}
          <!--<div class="form-group">
            <label for="">Seleccionar Usuario</label>
            <select class="form-control input-sm" name="id_usuario" required>
              <option value="1">Seleccione Usuario</option>
            </select>
          </div>-->
          <div class="form-group">
            <table class="table">
              @foreach($productos as $producto)
              <tr>
                <td width="20%"><label for="">Producto</label> <input type="text" size="4" class="form-control input-sm" name="nombre_producto" value="{{ $producto->nombre_producto }}" disabled><input type="hidden" name="producto[]" value="{{ $producto->id }}"> </td>
                <td class="success"><label for="">Cantidad</label><input type="text" class="form-control input-sm" size="2" name="cantidad[]" value="0" required></td>
                <td><label for="">Camiones</label>
                  <select class="form-control input-sm" name="camiones[]" required>
                        {{ ObtenerCamionesQueTenganElProductoSinCantidad(ObtenerNombreProducto($producto->id)) }}
                  </select></td>
                <td><label for="">Dirección</label><input type="text" class="form-control input-sm" name="direccion[]"></td>
                <td><label for="">Metodo Pago</label>
                  <select class="form-control input-sm" name="metodo_pago[]">
                      @foreach($metodos_pago as $metodopago)
                      <option value="{{ $metodopago->id }}">{{ $metodopago->nombre }}</option>
                      @endforeach
                  </select>
                </td>
              </tr>
              @endforeach
            </table>
          </div>
          <button type="submit" class="btn btn-success btn3d"><i class="fas fa-truck"></i> Asignar Despacho</button>
        </form>
      </div>
  </div>
@endsection
