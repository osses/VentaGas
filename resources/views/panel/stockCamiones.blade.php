@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-heading">Stock</div>
                <div class="panel-body">
                  @foreach($camiones as $camion)
                    <h5><b>Nombre Chofer: </b>{{ NombreUsuario($camion->fk_id_chofer) }}</h5>
                    <hr>
                    <h5>Stock disponible</h5>
                      <table class="table table-bordered">
                        <tr class="danger">
                          <th>Nombre Producto</th>
                          <th>Stock</th>
                        </tr>
                        @foreach($stocks as $stock)
                        <tr>
                          <td class="success">{{ $stock->nombre_producto }}</td>
                          <td class="info"><span class="btn btn-default btn-xs">{{ $stock->stock }}<span></td>
                        </tr>
                        @endforeach
                      </table>
                  @endforeach
                </div>
               </div>
          </div>
@endsection
