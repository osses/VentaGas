@extends('layouts.app')
@section('content')
<div class="col-md-9">
  @if (session('status'))
    <div class="alert alert-success">
      <p>{{ session('status') }}</p>
    </div>
    @endif
    <div class="panel panel-default">
    <div class="panel-heading">Productos</div>
    <div class="panel-body">
      <a href="/nuevo/producto" class="btn btn-success btn-sm btn3d">Agregar Producto</a> | <a href="/ingresar/factura" class="btn btn-danger btn-sm btn3d">Ingresar Factura</a>
      | <a href="/ver/facturas" class="btn btn-danger btn-sm btn3d">Ver Facturas</a>
      <hr>
        <table class="table table-bordered"  id="productos">
          <thead>
            <tr>
              <th>#</th>
              <!--<th>Imagen</th>-->
              <th>N. Producto</th>
              <!-- <th>Descripcion</th> -->
              <th>Categoria</th>
              <th>Stock</th>
              <th>Acción</th>
          </tr>
          </thead>
          <tbody>
            @foreach($productos_sub as $productos_s)
            <tr style="background-color:#464545;">
              <td>{{ $loop->iteration }}</td>
              <!--<td><img width="150" height="150" src="{{ URL::asset($productos_s->imagen_producto) }}" alt=""> </td>-->
              <td>{{ $productos_s->nombre_producto}}</td>
              <!--<td><textarea class="form-control" name="name">{{ $productos_s->descripcion_producto}}</textarea></td>-->
              <td>{{ ObtenerNCategoria($productos_s->fk_id_categoria) }}</td>
              <td>{{ $productos_s->stock - ProductoConFalla($productos_s->id) }}</td>
              <td><a href="/editar/producto/{{ $productos_s->id }}" class="btn btn-warning btn-xs btn3d">Editar</a> | <a href="/eliminar/producto/{{ $productos_s->id }}" class="btn btn-danger btn-xs btn3d">Eliminar</a> </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        </div>
      </div>
  </div>
<script type="text/javascript">
$(document).ready(function(){
  $('#productos').DataTable( {
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
        }
    });
});
</script>
@endsection
