@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-heading">Ingresar Factura</div>
                <div class="panel-body">
                  <form class="" action="" method="post">
                    {!! csrf_field() !!}
                    <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Nº Factura</label>
                        <input type="text" class="form-control input-sm" name="nfactura" value="" required>
                    </div></div>
                  <table class="table table-bordered"  id="productos">
                    <thead>
                      <tr>
                        <th>#</th>
                        <!--<th>Imagen</th>-->
                        <th>N. Producto</th>
                        <!-- <th>Descripcion</th> -->
                        <th>Categoria</th>
                        <th>Stock</th>
                        <th>Cantidad</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($productos as $productos)
                      <tr style="background-color:#464545;">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $productos->nombre_producto}}</td>
                        <td>{{ ObtenerNCategoria($productos->fk_id_categoria) }}</td>
                        <td>{{ $productos->stock - ProductoConFalla($productos->id) }}</td>
                        <td><input type="hidden" name="nombre_producto[]" value="{{ $productos->nombre_producto }}" required> <input type="text" class="form-control input-sm" name="cantidad[]" value="0" required> </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <button type="submit" class="btn btn-success btn3d">Ingresar Factura</button>
                </form>
                </div>
    </div>
</div>
@endsection
