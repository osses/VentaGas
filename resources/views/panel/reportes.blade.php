@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
                <div class="panel panel-default">
                <div class="panel-heading">Reportes</div>
                <div class="panel-body">
                  <div class="col-md-3">
                    <div class="panel panel-warning">
                    <div class="panel-heading">
                      <h3 class="panel-title">Cilindros con Fallas</h3>
                    </div>
                    <div class="panel-body">
                      <i class="fas fa-truck fa-2x"> </i> <span style="font-size:29px; margin-left:15px;">  {{ CilindrosConFallas() }}</span>
                    </div>
                  </div>
                  </div>
                  <div class="col-md-3">
                    <div class="panel panel-success">
                    <div class="panel-heading">
                      <h3 class="panel-title">Cilindros Entregados</h3>
                    </div>
                    <div class="panel-body">
                      <i class="fas fa-compass fa-2x"> </i> <span style="font-size:29px; margin-left:15px;">  {{ CilindrosEntregados() }}</span>
                    </div>
                  </div>
                  </div>
                  <div class="col-md-3">
                    <div class="panel panel-info">
                    <div class="panel-heading">
                      <h3 class="panel-title">Total Cilindros  Vacios</h3>
                    </div>
                    <div class="panel-body">
                      <i class="fas fa-truck fa-2x"> </i> <span style="font-size:29px; margin-left:15px;">  {{ CilindrosEntregados() }}</span>
                    </div>
                  </div>
                </div>
                  <div class="col-md-3">
                    <div class="panel panel-danger">
                    <div class="panel-heading">
                      <h3 class="panel-title">Total de Usuarios Registrados</h3>
                    </div>
                    <div class="panel-body">
                      <i class="fas fa-user fa-2x"> </i> <span style="font-size:29px; margin-left:15px;">  {{ UsuariosTotales() }}</span>
                    </div>
                  </div>
                </div>
                  <table class="table" id="despachos">
                    <hr>
                    <h4>Ultimas entregas concretadas:</h4>
                    <thead>
                      <tr class="well">
                        <th>#</th>
                        <th>Fecha</th>
                        <th>Dirección</th>
                        <th>Estado</th>
                        <th>Revisión</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($despachos as $despacho)
                        <tr class="default" style="background-color:#464545;">
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $despacho->created_at }}</td>
                          <td>{{ ObtenerDireccion($despacho->id) }}</td>
                          @if($despacho->estado == 0)
                              <td> <span style="width:100%;" class="btn btn-warning btn-xs btn3d">Por Entregar</span> </td>
                              @else
                              <td><span style="width:100%;" class="btn btn-success btn-xs btn3d">Entregado</span></td>
                          @endif
                          @if($despacho->estado == 0)
                              <td><a style="width:100%;" href="/despachos-check/{{ $despacho->id }}" class="btn btn-danger btn-xs btn3d">Pendiente</a> </td>
                              @else
                              <td><span style="width:100%;" class="btn btn-success btn-xs btn3d">OK</span></td>
                          @endif
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
          </div>
        </div>
    </div>
</div>
@endsection
