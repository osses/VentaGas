@extends('layouts.app')
@section('content')
  <div class="col-md-9">
    @if (session('status'))
      <div class="alert alert-success">
          <p>{{ session('status') }}</p>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Nuevo Producto / Cilindro</div>
                <div class="panel-body">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                    <div class="panel-body">
                      <form class="" action="/productos" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                          <label for="">Nombre Producto</label>
                          <input type="text" class="form-control" name="nombre_producto" value="" required>
                        </div>
                        <!--<div class="form-group">
                          <label for="">Descripción Producto</label>
                          <textarea class="form-control" name="descripcion_producto"></textarea>
                        </div>-->
                        <div class="form-group">
                          <label for="">Seleccione Categoria</label>
                          <select class="form-control" name="fk_id_categoria" required>
                            <option value="">Seleccionar...</option>
                            @foreach($categorias as $categoria)
                              <option value="{{ $categoria->id }}">{{ $categoria->nombre_categoria}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="">Stock</label>
                          <input type="number" class="form-control" name="stock" value="" required>
                        </div>
                        <!--<div class="form-group">
                          <label for="">Nº Factura (Opcional)</label>
                          <input type="number" class="form-control" name="numero_factura" value="">
                        </div>
                        <div class="form-group well">
                          <label for="">Imagen Producto</label>
                          <input type="file" name="imagen" value="">
                        </div>-->
                        <button type="submit"  class="btn btn-success btn3d">Agregar</button>
                        <a href="/productos"  class="btn btn-warning btn3d">Ir a Productos</a>
                      </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
</div>
@endsection
