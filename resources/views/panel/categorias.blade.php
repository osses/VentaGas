@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-heading">Categorias</div>
                <div class="panel-body">
                  <!--<div class="col-md-6">
                    <div class="panel panel-default">
                    <div class="panel-heading">Categorias</div>
                    <div class="panel-body">
                      <form class="" action="" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                          <label for="">Nombre Categoria</label>
                          <input type="text" class="form-control input-sm" name="nombre_categoria" value="">
                        </div>
                        <button type="submit"  class="btn btn-success">Agregar</button>
                        <button type="reset"  class="btn btn-warning">Reset</button>
                      </form>
                  </div>
                </div>
              </div>-->
                  <div class="col-md-12">
                    <table class="table table-bordered">
                          <thead>
                            <tr class="info">
                              <th>#</th>
                              <td>Nombre Categoria</td>
                              <th>Acción</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($categorias as $categoria)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $categoria->nombre_categoria }}</td>
                              <td><a href="/editar/categoria/{{ $categoria->id }}" class="btn btn-info btn-xs btn3d">Editar</a> <!--| <a href="/eliminar/categoria/{{ $categoria->id }}" class="btn btn-danger btn-xs">Eliminar</a> </td>-->
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
               </div>
          </div>
    </div>
</div>
@endsection
