@extends('layouts.app')
@section('content')
<div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
          </div>
          @endif
          <div class="panel panel-default">
                <div class="panel-heading">Mis Despachos</div>
                <div class="panel-body">
                  <table class="table" id="despachos">
                    <thead>
                      <th>#</th>
                      <th>Fecha</th>
                      <th>Dirección</th>
                      <th>Estado</th>
                      <th>Revisión</th>
                    </thead>
                    <tbody>
                      @foreach($despachos as $despacho)
                        <tr class="default" style="background-color:#464545;">
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $despacho->created_at }}</td>
                          <td>{{ ObtenerDireccion($despacho->id) }}</td>
                          @if($despacho->estado == 0)
                              <td> <span style="width:100%;" class="btn btn-warning btn-xs">Por Entregar</span> </td>
                              @else
                              <td><span style="width:100%;" class="btn btn-success btn-xs">Entregado</span></td>
                          @endif
                          @if($despacho->estado == 0)
                              <td><a style="width:100%;" href="/despachos-check/{{ $despacho->id }}" class="btn btn-danger btn-xs">Pendiente</a> </td>
                              @else
                              <td><span style="width:100%;" class="btn btn-success btn-xs">OK</span></td>
                          @endif
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
              </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#despachos').DataTable( {
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
        }
    });
});
</script>
@endsection
