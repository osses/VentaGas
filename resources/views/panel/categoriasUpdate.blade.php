@extends('layouts.app')
@section('content')
  <div class="col-md-9">
    @if (session('status'))
      <div class="alert alert-success">
          <p>{{ session('status') }}</p>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">Categorias</div>
                <div class="panel-body">
                  <div class="col-md-6">
                    <div class="panel panel-default">
                    <div class="panel-heading">Categorias</div>
                    <div class="panel-body">
                      @foreach($categorias as $categoria )
                      <form class="" action="/editar/categoria" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                          <label for="">Nombre Categoria</label>
                          <input type="hidden" name="id_categoria" value="{{ $categoria->id }}" value="">
                          <input type="text" class="form-control" value="{{ $categoria->nombre_categoria }}" name="nombre_categoria" value="">
                        </div>
                        <button type="submit"  class="btn btn-success btn3d">Actualizar</button>
                      </form>
                      @endforeach
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
</div>
@endsection
