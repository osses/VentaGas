@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-heading">Asignar Stock a Camiones de reparto</div>
                <div class="panel-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr class="success">
                        <th>#</th>
                        <th>Producto</th>
                        <th>Stock General</th>
                        <th>Camion</th>
                        <th>Cantidad</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($productos as $producto)
                      <tr>
                        <td><span class="btn btn-info btn-xs btn3d">{{ $loop->iteration }}</span></td>
                        <td><span class="btn btn-success btn-xs btn3d">{{ $producto->nombre_producto }}</span></td>
                        <td><span class="btn btn-danger btn-xs btn3d">{{ $producto->stock - ProductoConFalla($producto->id) }}</span></td>
                        <td>
                          <form class="" action="" method="post">
                            {!! csrf_field() !!}
                          <input type="hidden" name="nombre_producto" value="{{ $producto->nombre_producto }}" required>
                          <select class="form-control input-sm" name="id_camion">
                            @foreach($camiones as $camion)
                              <option value="{{ $camion->id }}">{{ $camion->patente }} | {{ ObtenerStockCamionesID($producto->nombre_producto, $camion->id)}}</option>
                            @endforeach
                          </select>
                        </td>
                        <td><input type="number" class="form-control input-sm" name="cantidad" value="0" required></td>
                        <td><button type="submit" class="btn btn-success btn-xs btn3d">Asignar</button></td>
                        </form>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
               </div>
          </div>
    </div>
@endsection
