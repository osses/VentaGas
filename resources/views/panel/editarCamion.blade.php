@extends('layouts.app')
@section('content')
  <div class="col-md-9">
    @if (session('status'))
      <div class="alert alert-success">
          <p>{{ session('status') }}</p>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                    <div class="panel-body">
                      <form class="" action="" method="post">
                        {!! csrf_field() !!}
                        @foreach($camiones as $camion)
                        <div class="form-group">
                          <label for="">Marca</label>
                          <input type="hidden" name="id_camion" value="{{ $camion->id }}">
                          <input type="text" class="form-control" name="marca" value="{{ $camion->marca }}">
                        </div>
                        <!--<div class="form-group">
                          <label for="">Descripción Producto</label>
                          <textarea class="form-control" name="descripcion_producto"></textarea>
                        </div>-->
                        <div class="form-group">
                          <label for="">Patente</label>
                          <input type="text" class="form-control" name="patente" value="{{ $camion->patente }}">
                        </div>
                        <!--<div class="form-group well">
                          <label for="">Imagen Producto</label>
                          <input type="file" name="imagen" value="">
                        </div>-->
                        <div class="form-group">
                          <label for="">Chofer</label>
                          <select class="form-control" name="chofer">
                              @foreach($usuarios as $user)
                                @if($camion->fk_id_chofer == $user->id)
                                  <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                                    @else
                                  <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endif
                              @endforeach
                          </select>
                        </div>
                        @endforeach
                        <button type="submit"  class="btn btn-success btn3d">Actualizar</button>
                        <a href="/camiones" class="btn btn-warning btn3d">Volver a Camiones</a>
                      </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
</div>
@endsection
