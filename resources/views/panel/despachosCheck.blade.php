@extends('layouts.app')
@section('content')
<div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
          </div>
          @endif
          <div class="panel panel-default">
                <div class="panel-heading">Revision de Despacho</div>
                <div class="panel-body">
                  <p class="alert alert-info">Si no selecciona un camión, automaticamente se descontara del stock general.</p>
                  @foreach($despachos as $despacho)
                    <table class="table table-bordered">
                        <tr>
                          <td>Fecha:</td>
                          <td colspan="5">{{ $despacho->created_at }}</td>
                        </tr>
                        <tr>
                          <td>Estado:</td>
                          @if($despacho->estado == 0)
                              <td colspan="5"> <span  class="btn btn-warning btn-xs btn3d">Por Entregar</span> </td>
                              @else
                              <td colspan="5"><span  class="btn btn-success btn-xs btn3d">Entregado</span></td>
                          @endif
                        </tr>
                        <tr>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>C. Entregada</th>
                          <th>Metodo de Pago</th>
                          <th>Dirección</th>
                          <th>Restar del Camión:</th>
                        </tr>
                        <form class="" action="" method="post">
                          {!! csrf_field() !!}
                        @foreach($productosDespachados as $productos)
                        @if($productos->cantidad > 0)
                        <tr>
                            <td><span style="width:100%;" class="btn btn-info btn-xs btn3d">{{ ObtenerNombreProducto($productos->fk_id_producto) }}</span></td>
                            <td><span style="width:100%;" class="btn btn-success btn-xs btn3d">{{ $productos->cantidad }}</span></td>
                            <td width="14%">
                              <input type="hidden" name="id_despacho" value="{{ $despacho->id }}">
                              <input type="hidden" name="id_producto[]" value="{{ $productos->fk_id_producto }}">
                              <input type="hidden" name="cantidad[]" value="{{ $productos->cantidad }}">
                              <input class="form-control input-sm" type="number" name="entregado[]" value="0">
                            </td>
                            <td><span class="btn btn-danger btn-xs btn3d">{{ ObtenerMetododePago($productos->metodo_pago) }}</span></td>
                            <td><span class="btn btn-danger btn-xs btn3d">{{ $productos->direccion }}</span></td>
                            <td>
                              <select class="form-control input-sm" name="restar_de">
                                    {{ ObtenerCamionesQueTenganElProducto(ObtenerNombreProducto($productos->fk_id_producto), $productos->cantidad) }}
                              </select>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                        <tr>
                          <td><button type="submit" class="btn btn-success btn3d"><i class="fas fa-truck"></i> Terminar Entrega</button></td>
                        </tr>
                      </form>
                    </table>
                  @endforeach
            </div>
    </div>
</div>
@endsection
