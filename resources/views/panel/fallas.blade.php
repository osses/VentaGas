@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-heading">Ingresar Cilindros con fallas</div>
                <div class="panel-body">
                  <h3>Ingresar Cilindros con fallas</h3>
                    <form class="" action="" method="post">
                      {{ csrf_field() }}
                      <div class="form-group">
                          <label for="">Seleccione Cilindro</label>
                          <select class="form-control input-sm" name="id_producto">
                            @foreach($productos as $producto)
                              <option value="{{ $producto->id }}">{{ $producto->nombre_producto }}</option>
                            @endforeach
                          </select>
                          </div>
                          <div class="form-group">
                            <label for="">Ingrese Cantidad</label>
                            <input type="number" class="form-control input-sm" name="cantidad" value="0">
                          </div>
                          <button type="submit" name="button" class="btn btn-success btn-sm btn3d">Ingresar</button>
                    </form>
                    <hr>
                    <h3>Devolver Cilindros con fallas</h3>
                      <form class="" action="/devolveraStock" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Seleccione Cilindro</label>
                            <select class="form-control input-sm" name="id_producto">
                              @foreach($productos_fallas as $producto)
                                <option value="{{ $producto->id }}">{{ ObtenerNombreProducto($producto->fk_id_producto) }} | {{ $producto->stock }}</option>
                              @endforeach
                            </select>
                            </div>
                            <div class="form-group">
                              <label for="">Ingrese Cantidad</label>
                              <input type="number" class="form-control input-sm" name="cantidad_falla" value="0">
                            </div>
                            <button type="submit" class="btn btn-success btn-sm btn3d">Devolver</button>
                      </form>
                    <!--<table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nombre Producto</th>
                          <th>Con fallas</th>
                          <th>Cantidad a devolver</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($productos_fallas as $producto)
                          <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ ObtenerNombreProducto($producto->fk_id_producto) }}</td>
                            <td>{{ $producto->stock }}</td>
                              <td><a class="btn btn-success btn-xs btn3d" href="/devolveraStock/$producto->fk_id_producto }}">Devolver a Stock</a></td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>-->
               </div>
          </div>
    </div>
@endsection
