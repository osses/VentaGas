@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-heading">Factura</div>
                <div class="panel-body">
                  @foreach($facturas as $factura)
                  <table class="table table-bordered">
                    <tr>
                      <td>Nº Factura</td>
                      <td><span class="btn btn-danger btn-xs btn3d">{{ $factura->numero_factura }}</span></td>
                    </tr>
                    <tr>
                      <td>Fecha de Ingreso</td>
                      <td><span class="btn btn-info btn-xs btn3d">{{ $factura->created_at }}</span></td>
                    </tr>
                    <tr>
                      <th>Productos Ingresados</th>
                      <td>
                        @foreach($productos as $producto)
                          <ul>
                            <li><span class="btn btn-danger btn-xs btn3d">{{ $producto->nombre_producto }}</span> | <span class="btn btn-success btn-xs btn3d">{{ $producto->stock }} Unidades</span></li>
                          </ul>
                        @endforeach
                      </td>
                    </tr>
                  </table>
                  @endforeach
               </div>
          </div>
    </div>
@endsection
