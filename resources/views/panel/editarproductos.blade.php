@extends('layouts.app')
@section('content')
  <div class="col-md-9">
    @if (session('status'))
      <div class="alert alert-success">
          <p>{{ session('status') }}</p>
            </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                  <div class="col-md-12">
                    <div class="panel panel-default">
                    <div class="panel-body">
                      <form class="" action="" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        @foreach($productos as $producto)
                        <div class="form-group">
                          <label for="">Nombre Producto</label>
                          <input type="hidden" name="id" value="{{ $producto->id }}">
                          <input type="text" class="form-control" name="nombre_producto" value="{{ $producto->nombre_producto }}" readonly>
                        </div>
                        <!--<div class="form-group">
                          <label for="">Descripción Producto</label>
                          <textarea class="form-control" name="descripcion_producto"></textarea>
                        </div>-->
                        <div class="form-group">
                          <label for="">Seleccione Categoria</label>
                          <select class="form-control" name="fk_id_categoria">
                            @foreach($categorias as $categoria)
                              @if($categoria->id == $producto->fk_id_categoria)
                                <option value="{{ $categoria->id }}" selected>{{ $categoria->nombre_categoria}}</option>
                              @else
                                <option value="{{ $categoria->id }}">{{ $categoria->nombre_categoria}}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="">Stock</label>
                          <input type="number" class="form-control" name="stock" value="{{ $producto->stock }}">
                        </div>
                        <!--<div class="form-group well">
                          <label for="">Imagen Producto</label>
                          <input type="file" name="imagen" value="">
                        </div>-->
                        @endforeach
                        <button type="submit"  class="btn btn-success btn3d">Actualizar</button>
                        <a href="/productos" class="btn btn-warning btn3d">Volver a Productos</a>
                      </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
</div>
@endsection
