@extends('layouts.app')
@section('content')
        <div class="col-md-9">
          @if (session('status'))
          <div class="alert alert-success">
              <p>{{ session('status') }}</p>
              </div>
                @endif
                <div class="panel panel-default">
                <div class="panel-heading">Facturas</div>
                <div class="panel-body">
                  <table class="table table-bordered" id=facturas>
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nº Factura</th>
                        <th>Producto</th>
                        <th>Cantidad Ingresada</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($facturas as $factura)
                      <tr style="background-color:#464545;">
                        <td>{{ $loop->iteration }}</td>
                        <td><a class="btn btn-danger btn-xs btn3d" href="/ver/factura/{{ $factura->numero_factura }}">{{ $factura->numero_factura }}</a> </td>
                        <td>{{ $factura->nombre_producto }}</td>
                        <td>{{ $factura->stock }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
        </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#facturas').DataTable( {
        "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
        }
    });
});
</script>
@endsection
