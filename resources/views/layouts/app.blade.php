<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <title>Gasco Ventas</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
     <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
     <!-- Latest compiled and minified JavaScript -->
     <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
     <script type="text/javascript" src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
     <script src="{{ URL::asset('js/all.js') }}"></script>
     <script src="{{ URL::asset('js/loader.js') }}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                      Gasco Ventas
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                          <!--  <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li> -->
                        @else
                            <li class="dropdown">
                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                     {{ Auth::user()->email }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @if (Auth::check())
        @if(Auth::User()->tipo_usuario == 2)
          <div class="container">
              <div class="row">
                <div class="col-md-3">
                  <div class="panel panel-default">
                      <div class="panel-heading">Menu</div>
                      <div class="panel-body">
                        <ul class="list-group">
                          <a href="/categorias" class="btn btn-success btn-sm btn3d" style="width:100%;"><i class="fas fa-align-justify"></i> Marcas / Categorias</a>
                          <a href="/productos" class="btn btn-success btn-sm btn3d" style="width:100%"><i class="fas fa-cart-arrow-down"></i> Stock General</a>
                          <a href="/asignar/stock" class="btn btn-success btn-sm btn3d" style="width:100%"><i class="far fa-newspaper"></i> Asignar Stock a Camiones</a>
                          <a href="/camiones" class="btn btn-success btn-sm btn3d" style="width:100%"><i class="far fa-newspaper"></i> Editar Camiones</a>
                          <a href="/despachos" class="btn btn-warning btn-sm btn3d" style="width:100%"><i class="fas fa-truck"></i> Realizar Despachos</a>
                          <a href="/IngresarFallas" class="btn btn-danger btn-sm btn3d" style="width:100%"><i class="fas fa-truck"></i> Ingresar fallas</a>
                          <!-- <a href="/checkear-despachos" class="btn btn-danger btn-sm btn3d" style="width:100%"><i class="fas fa-truck"></i> Checkear Despachos</a>-->
                          <a href="/usuarios" class="btn btn-success btn-sm btn3d" style="width:100%"><i class="fas fa-user"></i> Control de Usuarios</a>
                          <a href="/reportes" class="btn btn-success btn-sm btn3d" style="width:100%"><i class="fas fa-clone"></i> Reportes</a>
                          <a href="javascript:window.close();" class="btn btn-success btn-sm btn3d" style="width:100%"><i class="fas fa-key"></i> Cerrar</a>
                        </ul>
                      </div>
                  </div>
                </div>
                @elseif(Auth::User()->tipo_usuario == 1)
                <div class="container">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Menu</div>
                            <div class="panel-body">
                              <ul class="list-group">
                                <li class="list-group-item"><a href="/mis-despachos" class="btn btn-success btn-sm" style="width:100%">Mis Despachos</a></li>
                                <li class="list-group-item"><a href="javascript:window.close();" class="btn btn-success btn-sm" style="width:100%">Cerrar</a></li>
                              </ul>
                            </div>
                        </div>
                      </div>
                      @endif
                @endif
             @yield('content')
           </div>
        </div>
    </div>
    <!-- Scripts -->
</body>
</html>
