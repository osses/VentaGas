<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('login');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

// PANEL DE CONTROL

// CATEGORIAS
Route::get('/categorias', 'CategoriasController@index');
Route::post('/categorias', 'CategoriasController@indexPost');
Route::get('/editar/categoria/{id}', 'CategoriasController@update');
Route::post('/editar/categoria', 'CategoriasController@updatePost');
Route::get('/eliminar/categoria/{id}', 'CategoriasController@destroy');

// PRODUCTOS

Route::get('/productos','ProductosController@index');
Route::post('/productos','ProductosController@indexPost');
Route::get('/nuevo/producto', 'ProductosController@nuevoProducto');
Route::get('/editar/producto/{id}', 'ProductosController@update');
Route::post('/editar/producto/{id}', 'ProductosController@updatePost');
Route::get('/eliminar/producto/{id}', 'ProductosController@destroy');

// STOCK
Route::get('/stock', 'StockController@index');
Route::get('/despachos', 'StockController@despachos');
Route::post('/despachos', 'StockController@despachosPost');

// DESPACHOS

Route::get('/mis-despachos', 'DespachosController@misdespachos');
Route::get('/despachos-check/{id}', 'DespachosController@despachosCheck');
Route::post('/despachos-check/{id}', 'DespachosController@despachosCheckPost');
Route::get('/checkear-despachos', 'DespachosController@despachosCheckAdmin');

// USUARIOS
Route::get('/usuarios', 'HomeController@usuarios');
Route::get('/editar/usuario/{id}', 'HomeController@update');
Route::post('/editar/usuario/{id}', 'HomeController@updatePost');

// FALLAS

Route::post('/devolveraStock', 'HomeController@DevolveraStock');
Route::get('/IngresarFallas', 'HomeController@IngresarFalla');
Route::post('/IngresarFallas', 'HomeController@IngresarFallaPost');
// REPORTES

Route::get('/reportes', 'HomeController@reportes');

// Camiones
Route::get('/devolverstock', 'HomeController@devolverStock');
Route::get('/devolverstock/{id}', 'HomeController@devolverStockCamion');
Route::get('/camiones', 'HomeController@camiones');
Route::get('/editar/camion/{id}', 'HomeController@updateCamion');
Route::post('/editar/camion/{id}', 'HomeController@updateCamionPost');
Route::get('/stock/camion/{id}',  'HomeController@stockCamion');
Route::get('/asignar/stock/', 'HomeController@asignarStock');
Route::post('/asignar/stock/', 'HomeController@asignarStockPost');
Route::get('/ingresar/factura', 'HomeController@facturas');
Route::post('/ingresar/factura', 'HomeController@facturasPost');
Route::get('/ver/facturas', 'HomeController@verFacturas');
Route::get('/ver/factura/{id}', 'HomeController@verFacturasId');
